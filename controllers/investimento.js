const Investimento = require('../models/invertimentoModel');
const knex = require('../database/connection');
//teste de dados e api
class investimentoController{

    async read(req,res){
        res.status(200)
        knex('investimento').then(invert =>{
            res.json(invert)

        })
    }

    async create(req,res){
        var{nome, data_init,data_fim,valor, risco} = req.body;

        if(nome === undefined || data_init ===undefined || data_fim ===undefined || valor ===undefined || risco ===undefined){
            res.status(400);
            res.json({error: "Dados Incompletos"});
        }

        if(data_init > data_fim){

            res.status(400)
            res.json({error: "data inicio maior que data fim"})
        }

        res.status(200)
        await Investimento.new(nome, data_init,data_fim,valor, risco);
    }

    async update(req,res){
        try{
            let {id} = req.params;
            let {nome, data_init,data_fim,valor, risco} = req.body;
            await knex('investimento').where({id:id}).update({nome, data_init,data_fim,valor, risco});
            res.status(200);
        }catch(e){
            console.log(e)
        }
        return res.json(Investimento);
    }

    async delete(req,res){
        let {id} = req.params;
        try {
            await knex('investimento').where({id:id}).delete();
            res.status(200);
        }catch(error){
            res.status(400)
            res.json({error:error})
        }   
    }

    async detalheProjeto(req,res){
        let {id} =req.params;

        await knex.select(['investimento.nome','investimento.id']).table('investimento').where({id:id})
        .then(data =>{
            res.json(data)
        })
    }

    async retornoInvestimento(req,res){
        let {id} = req.params;
        let {valor_investimento} = req.body;
        let retorno = {valor:[]};
        let aux = 0;

        await knex.select(['investimento.risco'])
        .table('investimento')
        .where({id:id})
        .then(data =>{
           data.forEach(data =>{

                if(data.risco == 0){ 
                    aux = valor_investimento*0.5
                    retorno.valor.push(parseFloat(aux))
                    res.json(retorno)
                }
                if(data.risco == 1){
                    aux = valor_investimento*0.10
                    retorno.valor.push(parseFloat(aux))
                    res.json(retorno)
                }
                if(data.risco == 2){
                    aux = valor_investimento*0.20
                    retorno.valor.push(parseFloat(aux))
                    res.json(retorno)
                }
                
                
           })
            
        }).catch(e=>{
            res.sendStatus(400)
        })
    }
}

module.exports = new investimentoController();