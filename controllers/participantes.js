const Participante = require('../models/participanteModel')
const knex = require("../database/connection");
//MODAL REQUIRE

class participanteController{

    async read(req,res){
        res.status(200)
        knex('participante').select('nome').then(part =>{
            res.json(part)
        })
    }

    async create(req,res){

        var{nome,id_projeto} =req.body;

        if(nome === undefined){
            res.status(400);
            res.json({error: "Insira nome participantes"});
        }

        await Participante.new(nome, id_projeto);
    }



    async update(req,res){
        
        try{

            let {id} = req.params;
            let {nome, id_projeto} = req.body;
            await knex('participante').where({id:id}).update({nome,id_projeto});
            res.status(200);
        }catch(e){
            console.log(e);
        }

        return res.json(Participante);
    }

    async delete(req,res){
        let {id} = req.params;
        try {
            await knex('participante').where({id:id}).delete();
            res.status(200);
        }catch{
            return {status: false, error:"dados não atualizados"}
        }   
    }

    async ParticipanteInvestimento(req,res){
        var {id} =req.params; //id investimento
        var nome_part = {nome:[]}

        await knex.select(['participante.id_projeto','participante.nome','investimento.id'])
        .table('participante')
        .join('investimento',{'participante.id_projeto':'investimento.id'})
        .where({'investimento.id' : id})

        .then(data =>{
            data.forEach(nomes => {
                nome_part.nome.push(nomes.nome)
            })
            res.json(nome_part)
        }).catch(e =>{
            res.sendStatus(400)
        })

    }
}


module.exports = new participanteController();