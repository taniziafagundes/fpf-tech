
const api = axios.create({
    baseURL: "http://localhost:3000/",
});

function detalheProjeto(id){
    api.get("/investimento").then(response =>{
        let projetos = response.data;
        projetos.forEach(projeto =>{
            if(id == projeto.id){

                let div = document.getElementById(id);
                let conteudo = document.createElement('div');
                conteudo.className = "informacao"
                

                let newdata = projeto.data_init.substring(0,9).split("-").reverse().join('/');
                let data_fim = projeto.data_fim.substring(0,9).split("-").reverse().join('/');
            
                conteudo.innerHTML ="nome : "+ projeto.nome + " data inicio :  " + newdata +
                                    " data fim: " + data_fim +"  valor: " + projeto.valor +" risco: " +  projeto.risco;
                div.appendChild(conteudo);
            }
        })
    })
}

function simularInvestimento(id){

   
    api.get("/retorno/"+id).then(response =>{

        let investimento = response.data;
        let retorno = document.getElementById(id)
        let conteudo = documento.createElement('h6');
        conteudo.innerHTML = response.data;
        retorno.appendChild(conteudo)
        retorno.appendChild(response.data)

        console.log(response.data)
    })
}

function editProjeto(id){

}

function deleteProjeto(id){
    api.delete("/investimento/"+id).then(response =>{
        alert("gameDeletado")
    }).catch(error =>{
        console.log(error)
    })
}

api.get("/investimento").then(response=>{

    let projetos = response.data;
    const list = document.getElementById('list_investimento');

    projetos.forEach(invest => {
        let item = document.createElement('div');
        let conteudo = document.createElement('h5');
        item.className = "produto"
        item.id = invest.id;
        conteudo.innerHTML = invest.nome;
        item.appendChild(conteudo);
       
        let formInvestimento = document.createElement('form');
        conteudo = document.createElement('input')
        conteudo.nodeType = "number";
        conteudo.name = "valor_investimento"
        
        
        
        var simularBtn = document.createElement("button");
            simularBtn.innerHTML = "Simular Investimento";
            simularBtn.type="submit"
            simularBtn.className = "simular"

            simularBtn.addEventListener("click", function(){
                console.log("simular clicado")
                let produtoId = simularBtn.parentNode.id
                simularInvestimento(produtoId)
        })

        formInvestimento.appendChild(conteudo)
        formInvestimento.appendChild(simularBtn);
        var infoBtn = document.createElement("button");
            infoBtn.innerHTML = "Detalhe";
            infoBtn.className = "detalhe"
            infoBtn.addEventListener("click", function(){
                produtoId = infoBtn.parentNode.id
                detalheProjeto(produtoId)
        })

        var editBtn = document.createElement("button");
            editBtn.innerHTML = "Editar";
            editBtn.className = "editar"
            editBtn.addEventListener("click", function(){
                console.log("edit clicado")
                produtoId = editBtn.parentNode.id
                editProjeto(produtoId)
        })

        var deleteBtn = document.createElement("button")
            deleteBtn.innerHTML = "Deletar";
            deleteBtn.className ="delete"
            deleteBtn.addEventListener("click", function(){ 
                console.log("delet clicado")
                produtoId = deleteBtn.parentNode.id
                deleteProjeto(produtoId)
        })

        item.appendChild(formInvestimento);
        item.appendChild(infoBtn)
        item.appendChild(editBtn)
        item.appendChild(deleteBtn)
        list.appendChild(item);
            

    })

}).catch(e => {
    throw new Error(e);
})