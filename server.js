const express = require('express');
const app = express();
const router = require('./router/router');
const bodyParser =require("body-parser");
const cors = require("cors");

app.use(cors())
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


app.set('view engine','ejs')
app.use('/',router);



const PORT = process.env.PORT || 3300
app.listen(PORT, () =>{
    console.log("servidor rodando normalmente")
})

