const express = require('express')
const router = express.Router();

const investimentoController = require('../controllers/investimento');
const participanteController = require('../controllers/participantes');


router.get("/investimento",investimentoController.read);
router.post('/investimento/create',investimentoController.create);
router.put('/investimento/:id',investimentoController.update);
router.delete('/investimento/:id',investimentoController.delete);

router.get("/detalhe/:id",investimentoController.detalheProjeto)
router.get('/retorno/:id',investimentoController.retornoInvestimento);
router.get('/investimento/:id',participanteController.ParticipanteInvestimento);

router.get("/participantes",participanteController.read);
router.post('/participante/create',participanteController.create);
router.put('/participante/:id',participanteController.update);
router.delete('/participante/:id',participanteController.delete);

module.exports = router;