let knex = require('../database/connection');

class Investimento {
    async new(nome, data_init,data_fim, valor, risco){
        try{
            await knex.insert({nome, data_init,data_fim, valor, risco})
            .table("investimento");
        }catch(err){
            console.log(err)
        }
    }
}
module.exports = new Investimento();