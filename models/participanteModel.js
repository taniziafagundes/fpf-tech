let knex = require('../database/connection');

class Participante {
    async new(nome, id_projeto){
        try {
            await knex.insert({nome, id_projeto})
            .table("participante")
        }catch(err){
            console.log(err)
        }
    }
}
module.exports = new Participante();