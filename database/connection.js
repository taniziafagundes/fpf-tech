//mysql para teste da api
var knex = require('knex')({
    client: 'mysql2',
    connection: {
        host: 'localhost',
        user: 'root',
        password: '123456',
        database: 'investimentoFPF'
    }
});

module.exports = knex;

/* banco de dados clearDB do heroku

var knex = require('knex')({
    client: 'mysql2',
    connection: {
        host: 'us-cdbr-east.cleardb.com',
        user: 'adffdadf2341',
        password: 'adf4234',
        database: 'heroku_db'
    }
});

module.exports = knex;

//CLEARDB_DATABASE_URL => mysql://[username]:[password]@[host]/[database name]?reconnect=true
//DATABASE_URL: mysql://adffdadf2341:adf4234@us-cdbr-east.cleardb.com/heroku_db?reconnect=true

*/