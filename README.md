[[LinkedIn link]](https://www.linkedin.com/in/tanizia-fagundes-a45896125/)



<!-- PROJECT LOGO -->
<br />
<p align="center">
  
    <img src="readme-img/logo.png" alt="Logo" width="500" height="440">
  

  <h1 align="center">Documentação Repositorio API</h3>

  <H2 align="center">
     A Api está funcional.
    <br />
   



<hr>

<!-- ABOUT THE PROJECT -->
## Ferramentas necessárias para rodar localmente


Para rodar e testar localmente via Postman a API, foi necessario installar o mysql workbench e criar duas tables:

  Table 1 : investimento ([id,nome, data_init, data_fim, valor, risco])
  <br>
  Table 2 : participante ([id, nome,id_projeto])
  
  * tem uma arquivo .sql para importar o banco de dados.
  
  
* Postman para testar a API 
  <hr>

### NODE.JS
  #### Iniciando projeto 
  ``` npm init```
  
  
  <br>
  As dependencias ultilizadas no projeto foram: <br>

  
  * npm
  * express
  * axios
  * knex
  * body-parser
  * cors
  * mysql2
  
  #### para instalar uma dependência:
  <br>
  
  ```npm install nomeDependencia --save```

  #### com exceção do axios que e inserido com script
  ```<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js">		     </script>```
  
  <hr>
### Retorno das rotas da API, em arquivo JSON

*  Retorna um arquivo json com todos os projetos cadastardos<br>
```router.get("/investimento",investimentoController.read);```<br><br>
`ex retorno: {   
    "nome":"tanizia", 
    "data_init":"2020-11-11",
    "data_fim":"2021-12-11",
    "valor":32.000, 
    "risco":2
}`


*  cadastra um novo projeto  na base de dados, aceita aquivo json<br>
```router.post('/investimento/create',investimentoController.create);```

*  Atualiza projeto cadastrado<br>
```router.put('/investimento/:id',investimentoController.update);```

*  Deleta projeto selecionado<br>
```router.delete('/investimento/:id',investimentoController.delete);```

*  Retorna um arquivo json com todos os elementos da table projeto<br>
```router.get("/detalhe/:id",investimentoController.detalheProjeto)```

*  Retorna o resultado de um valor investido, inserido via formulario<br>
```router.get('/retorno/:id',investimentoController.retornoInvestimento);```

*  Retorna a lista de participantes de um projeto<br>
```router.get('/investimento/:id',participanteController.ParticipanteInvestimento);```

*  Retorna um arquivo json com todos os projetos cadastardos<br>
```router.get("/participantes",participanteController.read);```<br>
`ex retorno: {  "nome": "tanizia", 
    "id_projeto": 5
}`

*  Cadastro de participantes, ecebe como parametro do body, id_projeto e nome<br>
```router.post('/participante/create',participanteController.create);```

*  Atualiza cadastro dos participantes<br>
```router.put('/participante/:id',participanteController.update);```

*  Deleta participantes cadastrados<br>
```router.delete('/participante/:id',participanteController.delete);```

	

<hr>

<!-- GETTING STARTED -->
## Frontend

Para o front foi usado javascript, css e html sem Framework.
<hr>


### Configuracao mysql workbench local


   ```sh
   var knex = require('knex')({
    client: 'mysql2',
    connection: {
        host: 'localhost',
        user: 'seu_user',  
        password: 'sua_senha',
        database: 'investimentoFPF'
    }
});

module.exports = knex;
   ```


<hr>

<!-- USAGE EXAMPLES -->
## Documentações relacionadas

  Link para a documentação das tecnologias ultilizadas

 
#### npm __ [Documentation](https://www.npmjs.com/)
#### axios __ [Documentation](https://www.npmjs.com/package/axios)
#### node __ [Documentation](https://nodejs.org/pt-br/docs/)



<hr>


<!-- Git -->
## Git / gitlab



1.  faça o clone do repositorio na sua maquina local (`git@gitlab.com:taniziafagundes/fpf-tech.git`)
2. branchs master, frontend, backend
3. 3 branchs pois está no modelo gitflow

  <hr>
  
### Resultado de testes via Postman
  
   <img src="readme-img/retorno-inicio.png" alt="postman" width="900" height="500">
  
  <h4>Resultado da função simularInvestimento <br>
    
   <img src="readme-img/simularInvestimento.png" alt="postman" width="900" height="550">
    
     <h4>Resultado da rota `/investimento/create` na base de dados <br>
       
     <img src="readme-img/db.png" alt="mysql" width="900" height="550">



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.


<hr>

<!-- CONTACT0 -->
## Contacto

Tanizia fagundes - (tdlf@icomp.ufam.edu.br)
  
Linkdlin: [https://www.linkedin.com/in/tanizia-fagundes-a45896125/](https://www.linkedin.com/in/tanizia-fagundes-a45896125/)

Link projeto: [https://gitlab.com/taniziafagundes/fpf-tech#](https://gitlab.com/taniziafagundes/fpf-tech#e)


  
<!-- Meus Repositorios -->
#### ALGUNS DOS MEUS REPÓSITORIOS NO GITLAB PAGES joguinhos 
  
  
  Labirinto: [Jogar_labirito_Facil](https://taniziafagundes.gitlab.io/labirinto/)
  
  Joquenpo: [Jogar_JoKenPo](https://taniziafagundes.gitlab.io/jokenpo/)
  


